#include "mainwindow.h"

#include <QApplication>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    // initialize
    std::random_device rd = std::random_device {};
    w.rng = std::default_random_engine { rd() };
    w.default_icon = QIcon(":/Icons/question_mark.png");
    w.revealed_num = 0;
    w.first = -1;
    w.second = -1;

    //Load the icons
    w.icons =
    {
        QIcon(":/Icons/apple.png"),
        QIcon(":/Icons/banana.png"),
        QIcon(":/Icons/blueberry.png"),
        QIcon(":/Icons/cherry.png"),
        QIcon(":/Icons/coconut.png"),
        QIcon(":/Icons/kiwi.png"),
        QIcon(":/Icons/lemon.png"),
        QIcon(":/Icons/mango.png"),
        QIcon(":/Icons/orange.png"),
        QIcon(":/Icons/passionfruit.png"),
        QIcon(":/Icons/peach.png"),
        QIcon(":/Icons/pear.png"),
        QIcon(":/Icons/pineapple.png"),
        QIcon(":/Icons/pomegranate.png"),
        QIcon(":/Icons/strawberry.png"),
        QIcon(":/Icons/watermelon.png"),
        QIcon(":/Icons/fig.png"),
        QIcon(":/Icons/dragonfruit.png")
    };

    //Load the buttons
    w.buttons.reserve(NUM_BUTTONS);
    w.button_px_len = (WINDOW_WIDTH_HEIGHT - SPACING * (BUTTON_SIDE_LEN + 1)) / BUTTON_SIDE_LEN;
    for(int i = 0; i < NUM_BUTTONS; i++)
    {
        w.buttons.append(new QPushButton("", &w));
        w.buttons[i]->setGeometry(
            QRect(
                QPoint(
                    (i % BUTTON_SIDE_LEN + 1) * SPACING + i % BUTTON_SIDE_LEN * w.button_px_len,
                    (i / BUTTON_SIDE_LEN + 1) * SPACING + i / BUTTON_SIDE_LEN * w.button_px_len
                ),
                QSize(
                    w.button_px_len,
                    w.button_px_len
                )
            )
        );
    }

    // icon placement
    for(int i = 0; i < NUM_BUTTONS; i+=2)
    {
        w.icon_placement.append(i/2);
        w.icon_placement.append(i/2);
    }

    // shuffle icons and set them on appropriate buttons
    std::shuffle(std::begin(w.icon_placement), std::end(w.icon_placement), w.rng);
    for(int i = 0; i < NUM_BUTTONS; i++)
    {
        w.solved_ids.append(false);
        w.buttons[i]->setIcon(w.default_icon);
        w.buttons[i]->setAutoRepeatDelay(i); //Used this as an id container.
        w.buttons[i]->setIconSize(QSize(w.button_px_len - 10, w.button_px_len - 10));
        QObject::connect(w.buttons[i], &QPushButton::released, &w, &MainWindow::handleButton);
    }
    w.show();

    // start application
    return a.exec();
}
