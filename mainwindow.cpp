#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDir"
#include <QDebug>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::handleButton()
{
    int clicked_button_id = -1;
    //Get the id of the clicked button
    QPoint mouse_pos = QWidget::mapFromGlobal(QCursor::pos());
    for (int i = 0; i < NUM_BUTTONS; i++) {
        // check if position is in a button bounding box
        if(QRect(
            QPoint(
                (i % BUTTON_SIDE_LEN + 1) * SPACING + i % BUTTON_SIDE_LEN * button_px_len,
                (i / BUTTON_SIDE_LEN + 1) * SPACING + i / BUTTON_SIDE_LEN * button_px_len
            ),
            QSize(
                button_px_len,
                button_px_len
            )
        ).contains(mouse_pos))
        {
            clicked_button_id = i;
            break;
        }
    }

    // No button was clicked
    if(clicked_button_id < 0)
    {
        return;
    }

    // clicked on a button of a revealed double
    if(solved_ids[clicked_button_id])
    {
        // check if everything is solved
        for(int i = 0; i < NUM_BUTTONS; i++)
        {
            if(!solved_ids[i])
            {
                return;
            }
        }
        // If the code gets here, it means the game is finished, therefore reset the game.
        for(int i = 0; i < NUM_BUTTONS; i++)
        {
            solved_ids[i] = false;
            buttons[i]->setIcon(default_icon);
        }
        std::shuffle(std::begin(icon_placement), std::end(icon_placement), rng);
    }
    else
    {
        // clicked on a revealed button
        if(clicked_button_id == first || clicked_button_id == second)
        {
            return;
        }

        // clicked on some third unrevealed button
        if(revealed_num == 2)
        {
            // flip the sides back if the choice was not a pair
            if(!solved_ids[first] && !solved_ids[second])
            {
                buttons[first]->setIcon(default_icon);
                buttons[second]->setIcon(default_icon);
            }
            revealed_num = 0;
            first = -1;
            second = -1;
        }
        // Reveal the button
        buttons[clicked_button_id]->setIcon(icons[icon_placement[clicked_button_id]]);

        // Safe the ids of chosen buttons and check for a pair
        if(first == -1)
        {
            first = clicked_button_id;
        }
        else
        {
            second = clicked_button_id;
            if (icon_placement[first] == icon_placement[second])
            {
                solved_ids[first] = true;
                solved_ids[second] = true;
            }
        }
        revealed_num++;
    }


}

MainWindow::~MainWindow()
{
    delete ui;
}
