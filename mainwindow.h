#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <random>

#define NUM_BUTTONS 36
#define WINDOW_WIDTH_HEIGHT 900
#define SPACING 30
#define BUTTON_SIDE_LEN 6

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QVector<QPushButton*> buttons; //Buttons container
    QVector<QIcon> icons; //Icons container, contains loaded images
    QVector<int> icon_placement; //Shows the actual placement of icons among the buttons
    QVector<bool> solved_ids; //Determines what buttons are considered solved
    QIcon default_icon; // So called "back" of the card. Unrevealed button.
    int button_px_len; // Button side length in pixels
    int revealed_num; // Number of uncovered buttons
    int first; // Id of the first unrevealed button
    int second; // Id of the second unrevealed button
    std::default_random_engine rng; // Engine for random shuffling
private:
    Ui::MainWindow *ui;

public slots:
    void handleButton();
};
#endif // MAINWINDOW_H
